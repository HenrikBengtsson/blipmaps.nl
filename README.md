# blipmaps.nl

This project is to manage and develop the weather forecasts on https://blipmaps.nl which are done using RASP

Howto run a forecast:

[Install a modern docker](https://docs.docker.com/install/linux/docker-ce/debian/) and [install a modern docker-compose](https://docs.docker.com/compose/install/). Usually the OS provided ones are too old (as of January '20)

Clone/download files to the repository:
```
git clone https://gitlab.com/DavidRasp/blipmaps.nl.git
cd blipmaps.nl
./download_binaries.sh
```
Ensure the public/private keypair etc are available (if you don't upload files as a final step; just touch them):
```
secrets:
  host_ssh_key:
    file: ~/.ssh/id_ed25519
  host_ssh_pub_key:
    file: ~/.ssh/id_ed25519.pub
  host_ssh_known_hosts:
    file: ~/.ssh/known_hosts
```

Build image and create two directories to see the results:
```
docker-compose -f docker-compose.yml build netherlands0  #only need to do this once
mkdir /tmp/OUT && mkdir /tmp/LOG
```

Run e.g. Netherlands today (see netherlands0 in docker-compose file):
```
docker-compose -f docker-compose.yml run netherlands0
```
If you want to upload the images afterwards, set the variable "targetUrl" to resemble something like "user@host:/home/user/domains/somerasp.org/images/"

After that, 
- You can find the images in the /tmp/OUT/<date>/<time>/<region>/<offset> folder
- The images can be served by https://github.com/dingetje/RASPViewer:
  - The corners.js file must be adjusted to match the logfiles which are named ncl.out.02.xxx
  - Ensure you adjust the server name in index.html

Some things to do:
Goals:
  - Split binaries / files which are never adjusted and put them on docker.blipmaps.nl
    - Put config files in Gitlab 
  - Create OSM sync script (download OSM & put that on docker.blipmaps.nl)
  
Extra goals:
  - Have dinner together
  - Add XCSoar support

## Meteogram
You can locate the locations which are used to generate a meteogram in the file sitedata.ncl

## Post processing
After you are done with generating the forecast and its images, we can do postprocessing. Because I wanted to automate that I've used a separate container. See the callback directory for that. 
